# FRONT-END

## Dependencies
Install Node/npm if needed.

Install project dependencies.
```shell script
cd <directory>
npm install
```

### Usage
Start server.
 
```shell script
npm start
```

Nodejs might need to be updated if `npm start` throws an error.
