import {Sector, UserFormState, ValidationError} from "./Types";

export function isSelected(id: number, selectedSectors: Sector[]): boolean {
  return !!selectedSectors.find((sector) => sector.id === id);
}

export function addPadding(depth: number) {
  return "- ".repeat(depth - 1);
}

export function getValidationClass(errors: ValidationError[], fieldName: string) {
  return getErrorMessagesByField(errors, fieldName).length > 0 ? "is-invalid" : "";
}

export function parseHTLMOptionsToSectors(options: HTMLOptionElement[], sectors: Sector[]) {
  const selectedSectors: Sector[] = [];

  for (const option of options) {
    if (option.selected) {
      const sector = sectors.find((s) => s.id === Number(option.value));
      if (sector) {
        selectedSectors.push(sector);
      }
    }
  }

  return selectedSectors;
}

export function createInitialState(): UserFormState {
  return {
    form: {
      name: "default",
      sectors: [],
      agreementAccepted: false,
    },
    saved: false,
    sectors: [],
  };
}

export function getErrorMessagesByField(errors: ValidationError[], fieldName: string) {
  const messages: string[] = [];
  errors.forEach((error) => {
    if (error.field === fieldName) {
      messages.push(error.message);
    }
  });

  return messages;
}
