export interface Sector {
    id: number;
    name: string;
    depth: number;
}

export interface UserForm {
    id?: number;
    name: string;
    sectors: Sector[];
    agreementAccepted: boolean;
    errors?: any;
}

export interface UserFormState {
    form: UserForm;
    saved: boolean;
    sectors: Sector[];
    errors?: ValidationError[];
}

export interface ValidationError {
    field: string;
    message: string;
}
