import * as React from "react";
import UserForm from "./components/UserForm";

const App = () => {
  return (
    <div className="container">
      <UserForm />
    </div>
  );
};

export default App;
