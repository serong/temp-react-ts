import {Sector, ValidationError} from "../Types";
import * as React from "react";
import FormError from "./FormError";
import {
  addPadding,
  getErrorMessagesByField,
  getValidationClass,
  parseHTLMOptionsToSectors,
} from "../Helpers";

interface SectorsProps {
  sectors: Sector[];
  selectedSectors: Sector[];
  errors?: ValidationError[];

  handleSelection(sectors: Sector[]): void;
}

export default function Sectors(props: SectorsProps) {

  function handleSelection(event: any, sectors: Sector[]) {
    event.preventDefault();

    const selectedSectors = parseHTLMOptionsToSectors(event.target.options, sectors);
    props.handleSelection(selectedSectors);
  }

  return (
    <>
      <div className="row">
        <label htmlFor="user-sectors">Sectors: </label>

        <select name="sectors" id="user-sectors" size={8} multiple={true}
                className={`form-control ${getValidationClass(props.errors || [], "sectors")}`}
                onChange={(e) => handleSelection(e, props.sectors)}>

          {props.sectors.map((sector) => {
            return (
              <option key={sector.id} value={sector.id}>
                {addPadding(sector.depth)} {sector.name}
              </option>);
          })}

        </select>
      </div>
      <FormError key="sector-error"
                 messages={getErrorMessagesByField(props.errors || [], "sectors")}/>
    </>
  );
}
