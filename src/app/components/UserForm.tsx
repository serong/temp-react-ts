import React from "react";
import {Sector, UserForm as UserFormType, UserFormState, ValidationError} from "../Types";
import Sectors from "./Sectors";
import {createInitialState, getErrorMessagesByField, getValidationClass} from "../Helpers";
import FormError from "./FormError";

function getSectors(): Promise<Sector[]> {
  return fetch("http://localhost:8082/api/sector/all")
    .then((response) => response.json());
}

function saveForm(form: UserFormType): Promise<UserFormType> {
  return fetch("http://localhost:8082/api/user-form/", {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(form),
  }).then((resp) => resp.json());
}

function editForm(form: UserFormType): Promise<UserFormType> {
  const url = `http://localhost:8082/api/user-form/${form.id}`;
  return fetch(url, {
    method: "PATCH",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(form),
  }).then((resp) => resp.json());
}

class UserForm extends React.Component<{}, UserFormState> {

  constructor(props: {}) {
    super(props);

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleFormEdit = this.handleFormEdit.bind(this);
    this.handleSectorSelection = this.handleSectorSelection.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);

    this.state = createInitialState();
  }

  public componentDidMount() {
    getSectors().then((resp) => {
      this.setState({sectors: resp});
    });
  }

  public render() {
    return (
      <div className="row justify-content-md-center">
        <div className="col-md-5 mt-5 card">

          <div className="row card-header">
            <p>Please enter your name and pick the Sectors you are currently involved in.</p>
          </div>

          <form className="m-3" onSubmit={this.handleFormSubmit}>
            <div className="row">
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <label className="input-group-text" id="basic-addon1" htmlFor="form-name">Name</label>
                </div>
                <input type="text" id="form-name" placeholder="Username"
                       className={`form-control ${getValidationClass(this.state.errors || [], "name")}`}
                       value={this.state.form.name} onChange={this.handleNameChange}
                       aria-label="Username" aria-describedby="basic-addon1"/>
              </div>
            </div>
            <FormError key="name-error"
                       messages={getErrorMessagesByField(this.state.errors || [], "name")}/>

            <Sectors sectors={this.state.sectors} errors={this.state.errors}
                     selectedSectors={this.state.form.sectors}
                     handleSelection={this.handleSectorSelection}/>

            <div className="row">
                <div className="form-check">
                  <input name="agreementAccepted" type="checkbox" id="form-agreement"
                         className={`form form-check-input ${getValidationClass(this.state.errors || [], "agreementAccepted")}`}
                         checked={this.state.form.agreementAccepted} onChange={this.handleCheckboxChange}/>
                  <label className="form-check-label" htmlFor="form-agreement"> Agree to terms </label>
                </div>
            </div>
            <FormError key="agreement-error"
                       messages={getErrorMessagesByField(this.state.errors || [], "agreementAccepted")}/>

            <div className="row justify-content-md-end">
              {!this.state.saved ?
                <button type="submit" className="btn btn-outline-primary">Save</button> :
                <button type="button" className="btn btn-outline-success" onClick={this.handleFormEdit}>Update</button>}
            </div>
          </form>
        </div>
      </div>
    );
  }

  private handleFormSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();

    saveForm(this.state.form).then((response) => {
        if (response.errors) {
          const errors: ValidationError[] = [];
          response.errors.map((err: any) => errors.push({field: err.field, message: err.defaultMessage}));
          this.setState({errors});
        } else {
          this.setState({form: response, saved: true, errors: []});
        }
      },
    );
  }

  private handleFormEdit(event: React.MouseEvent<HTMLElement>): void {
    event.preventDefault();

    editForm(this.state.form).then((response) => {
        if (response.errors) {
          const errors: ValidationError[] = [];
          response.errors.map((err: any) => errors.push({field: err.field, message: err.defaultMessage}));
          this.setState({errors});
        } else {
          this.setState({form: response, saved: true, errors: []});
        }
      },
    );
  }

  private handleSectorSelection(selectedSectors: Sector[]) {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        sectors: selectedSectors,
      },
    });
  }

  private handleNameChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      form: {
        ...this.state.form,
        name: event.target.value,
      },
    });
  }

  private handleCheckboxChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      form: {
        ...this.state.form,
        agreementAccepted: event.target.checked,
      },
    });
  }
}

export default UserForm;
