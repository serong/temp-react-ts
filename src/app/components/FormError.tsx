import * as React from "react";

interface FormErrorErrorProps {
  messages: string[];
}

export default function FormError(props: FormErrorErrorProps) {

  return (
    <div className="row">
      {props.messages.map((message, ind) => {
        return (
          <div key={ind} className="row"><div className="col text-danger mb-1 small">{message}</div></div>
        );
      })}
    </div>
  );
}
